import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://edikdolynskyi.github.io/react_sources/messages.json',
});

export const chatAPI = {
  getChatData (id = 'b919cb46edac4c74d0a8') {
    return instance.get().then(res => {
      return res.data
    }).catch(err=> JSON.parse(err.request.responseText));
  },
};
