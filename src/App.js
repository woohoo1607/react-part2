import React from 'react';
import './App.css';
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import ChatContainer from "./components/Chat/ChatContainer";

function App() {
  return (
    <div className="App">
      <Header />
      <ChatContainer />
      <Footer />
    </div>
  );
}

export default App;
