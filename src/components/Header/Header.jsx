import React from 'react';
import Logo from '../../image/messengerLogo.png';
import './styles.css';

const Header = () => {
  return (
      <header>
        <div className="center">
          <img src={Logo} height="70px" alt="logo" />
          <h2>Messenger</h2>
        </div>
        <div className='clr'></div>
      </header>
  )
};
export default Header;
