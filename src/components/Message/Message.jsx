import React, {useState, useEffect} from 'react';
import Moment from 'react-moment';
import 'moment-timezone';

import Like from '../../image/like.png'
import './styles.css';
import Modal from "../Modal/Modal";

const Message = ({message, myId, editMsg, deleteMsg, liked, messageLikes, isSuccessEdit, openModal, closeModal, isShowModal, ...props}) => {

  const upKeyPress = (e) => {
    if(e.key==='ArrowUp') {
      openModal();
    }
  }

  useEffect(() => {
    if (isSuccessEdit) {
      document.addEventListener("keydown", upKeyPress);
    }
    return () => {
      document.removeEventListener("keydown", upKeyPress);
    }
  },[isSuccessEdit]);

  let [isMouseEnter, setIsMouseEnter] = useState(false);
  let [msg, setMsg] = useState(message.text);
  let isLiked = false;
  if (messageLikes.find(like=> like.userId===myId)) {
    isLiked=true
  }

  const mouseEnter = () => {
    setIsMouseEnter(true);
  }

  const mouseLeave = () => {
    setIsMouseEnter(false);
  }

  const cancelEdit = () => {
    setMsg(message.text);
    closeModal();
  }

  const saveMsg = () => {
    editMsg(message.id, msg)
    closeModal();
  }

  const deleteMessage = () => {
    deleteMsg(message.id);
  }

  const changeInput = (e)  => {
    setMsg(e.target.value);
  }

  const likeMsg = () => {
    liked(message.id, myId)
  }

  let isMyMsg = message.userId===myId ? true : false;
  return (
      <>
        {isShowModal && isSuccessEdit && <Modal msg={msg} changeInput={changeInput} saveMsg={saveMsg} cancelEdit={cancelEdit}/>}
      <div className={isMyMsg ? 'message right' : 'message'} onMouseEnter={mouseEnter} onMouseLeave={mouseLeave}>
        {!isMyMsg && <img src={message.avatar} alt='avatar'/>}
        <div className='bodyMessage'>
          <div className='bodyMessageTitle'>
            <h3>{message.user}</h3>
            <p><Moment format='hh:mm:ss' date={message.createdAt}/></p>
            {isMyMsg && <>
              {isSuccessEdit && isMouseEnter && <button onClick={openModal}>edit</button>}
                <button onClick={deleteMessage}>delete</button>
              </>
            }
          </div>
          <p>{message.text}</p>
          <div className='messageInfo'>
            {isLiked && <span>you like</span>}
            {!isMyMsg && <img onClick={likeMsg} src={Like} height='10px' alt='like'/>}
          </div>
        </div>
      </div>
        </>
  )
};
export default Message;
