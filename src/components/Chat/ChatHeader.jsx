import React from 'react';
import Moment from 'react-moment';
import 'moment-timezone';

const ChatHeader = ({title, quantityUsers, quantityMessages, lastTimeMsg, ...props}) => {
  return (
      <div className='chatHeader'>
        <p className='chatName'>{title}</p>
        <p>{quantityUsers} participants</p>
        <p>{quantityMessages} messages</p>
        <p className='time'>last message at <Moment format='hh:mm:ss' date={lastTimeMsg} /></p>
      </div>
  )
};
export default ChatHeader;
