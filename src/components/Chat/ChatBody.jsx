import React, {useEffect, useRef} from 'react';

import Message from "../Message/Message";
import TimeLine from "../TimeLine/TimeLine";

const ChatBody = ({messages, myId, editMsg, deleteMsg, liked, likes, openModal, closeModal, isShowModal, ...props}) => {
  const messagesEndRef = useRef(null);
  let timelineDate = 0;
  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" })
  };
  const myPosts = messages.filter(message=>message.userId===myId);
  const myLastPost = myPosts[myPosts.length-1];
  const myLastPostId = myLastPost ? myLastPost.id : undefined

  useEffect(scrollToBottom, [messages]);
  return (
      <div className='chatBody'>
        {messages.map(message => {
          let isSuccessEdit = false;
          if (myLastPostId && message.id===myLastPostId) {
            isSuccessEdit = true;
          }
          const messageLikes = likes.filter(like=>like.msgId===message.id);
          const date = new Date(message.createdAt);
          const msgDay = date.getDay();
          if (msgDay!==timelineDate) {
            timelineDate=msgDay;
            return (
                <>
                  <TimeLine date={date} key={date}/>
                  <Message key={message.id}
                           message={message}
                           myId={myId}
                           editMsg={editMsg}
                           deleteMsg={deleteMsg}
                           liked={liked}
                           messageLikes={messageLikes}
                           isSuccessEdit={isSuccessEdit}
                           openModal={openModal}
                           closeModal={closeModal}
                           isShowModal={isShowModal}
                  />
                </>
            )
          }
          return <Message key={message.id}
                   message={message}
                   myId={myId}
                   editMsg={editMsg}
                   deleteMsg={deleteMsg}
                   liked={liked}
                   messageLikes={messageLikes}
                   isSuccessEdit={isSuccessEdit}
                   openModal={openModal}
                   closeModal={closeModal}
                   isShowModal={isShowModal}
          />
        })}
        <div ref={messagesEndRef}></div>
      </div>
  )
};
export default ChatBody;
