import React, {useEffect} from "react";
import {connect} from "react-redux";
import Chat from "./Chat";
import {getUser} from "../../reducers/userReducer";
import {getMessages, setIsShowModal, setLikes, setMessages} from "../../reducers/messengerReducer";
import {getDate} from "../../helpers/helpers";
import {uuid} from "uuidv4";

const ChatContainer = (props) => {
  useEffect(() => {
    props.getMessages();
    props.getUser();
  },[])

  const openModal = () => {
    props.setIsShowModal(true)
  }

  const closeModal = () => {
    props.setIsShowModal(false)
  }

  const editMsg = (msgId, text) => {
    let copyMessages = [...props.messages];
    const msgIndex = copyMessages.findIndex(message=>message.id===msgId);
    copyMessages[msgIndex].text = text;
    copyMessages[msgIndex].editedAt = getDate();
    props.setMessages(copyMessages);
  }

  const deleteMsg = (msgId) => {
    let copyMessages = [...props.messages];
    const msgIndex = copyMessages.findIndex(message=>message.id===msgId);
    copyMessages.splice(msgIndex, 1);
    props.setMessages(copyMessages);
  }

  const sendMsg = (msg) => {
    const newMessage = {
      id: uuid(),
      text: msg,
      avatar: props.user.avatar,
      user: props.user.name,
      userId: props.user.id,
      editedAt: "",
      createdAt: getDate()
    }
    let newMessages = [...props.messages]
    newMessages.push(newMessage);
    props.setMessages(newMessages);
  }

  const liked = (msgId, userId) => {
    let copyLikes = [...props.likes];
    const likeIndex = copyLikes.findIndex(like=> like.msgId===msgId && like.userId===userId);
    if (likeIndex===-1) {
      const newLike = {
        msgId,
        userId,
        isLike: true,
      };
      copyLikes.push(newLike);
      props.setLikes(copyLikes);
    } else {
      copyLikes.splice(likeIndex, 1);
      props.setLikes(copyLikes);
    }
  }

  return (
      <Chat myId={props.user.id}
            messages={props.messages}
            likes={props.likes}
            isFetching={props.isFetching}
            editMsg={editMsg}
            deleteMsg={deleteMsg}
            sendMsg={sendMsg}
            liked={liked}
            closeModal={closeModal}
            openModal={openModal}
            isShowModal={props.isShowModal}
      />
  )
};

let mapStateToProps = (state) => {
  return {
    user: state.user.user,
    messages: state.messenger.messages,
    likes: state.messenger.likes,
    isFetching: state.messenger.isFetching,
    isShowModal: state.messenger.isShowModal,
  }
};

export default connect(mapStateToProps, {getUser, getMessages, setMessages, setLikes, setIsShowModal})(ChatContainer);
