import React from 'react';

import './styles.css';
import ChatFooter from "./ChatFooter";
import ChatHeader from "./ChatHeader";
import {unique} from "../../helpers/helpers";
import ChatBody from "./ChatBody";
import Preloader from "../Preloader/Preloader";

const Chat = ({myId, messages, likes, isFetching, editMsg, deleteMsg, sendMsg, liked, openModal, closeModal, isShowModal}) => {

  const quantityUsers = unique(messages.map(user => user.userId)).length;
  const lastTimeMsg = messages.length ? messages[messages.length - 1].createdAt : false;

  return (
      <div className='center'>
        <div className='chatContainer'>
          {isFetching && <Preloader />}
          {!isFetching && <>
            <ChatHeader title='My chat'
                        quantityUsers={quantityUsers}
                        quantityMessages={messages.length}
                        lastTimeMsg={lastTimeMsg}
            />
            <ChatBody messages={messages}
                      myId={myId}
                      editMsg={editMsg}
                      deleteMsg={deleteMsg}
                      liked={liked}
                      likes={likes}
                      openModal={openModal}
                      closeModal={closeModal}
                      isShowModal={isShowModal}
            />
            <ChatFooter sendMsg={sendMsg}/>
          </>
          }
        </div>
      </div>
  )
};

export default Chat;
