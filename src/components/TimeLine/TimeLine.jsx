import React from 'react';
import Moment from 'react-moment';
import 'moment-timezone';

import './styles.css';
import moment from "moment-timezone";

const TimeLine = ({date}) => {
  let day = moment(date).calendar().split(' at')[0];
  if (day!=='Today' && day!=='Yesterday') {
    day = false;
  }
  return (
      <div className='timeline'>
        <span>
          {!day && <Moment format='YYYY-MM-DD' date={date}/>}
          {day && <time>{day}</time>}
        </span>
      </div>
  )
}
export default TimeLine;
