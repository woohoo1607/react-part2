import React from 'react';

import loading from './Preloader.gif';
import './styles.css';

const Preloader = () => {
  return (
      <div className='preloader'>
        <img src={loading} alt='preloader' />
      </div>
  )
};

export default Preloader;
