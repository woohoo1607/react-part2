import {applyMiddleware, combineReducers, createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import userReducer from "../reducers/userReducer";
import messengerReducer from "../reducers/messengerReducer";

let reducers = combineReducers({
  user: userReducer,
  messenger: messengerReducer,
});

let store = createStore(reducers, applyMiddleware(thunkMiddleware));

export default store;
