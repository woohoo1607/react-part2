import {chatAPI} from "../api/api";
import {sortByTime} from "../helpers/helpers";

const SET_IS_FETCHING = 'SET_IS_FETCHING';
const SET_MESSAGES = 'SET_MESSAGES';
const SET_LIKES = 'SET_LIKES';
const SET_IS_SHOW_MODAL = 'SET_IS_SHOW_MODAL';

let initialState = {
  messages: [],
  likes: [],
  isFetching: false,
  isShowModal: false,
};

const messengerReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_MESSAGES:
    {
      return {
        ...state,
        messages: [...action.messages]
      };
    }
    case SET_LIKES:
    {
      return {
        ...state,
        likes: [...action.likes]
      };
    }
    case SET_IS_FETCHING:
    {
      return {
        ...state,
        isFetching: action.isFetching
      };
    }
    case SET_IS_SHOW_MODAL: {
      return {
        ...state,
        isShowModal: action.isShowModal
      };
    }
    default:
      return state;
  }
};

export const setMessages = (messages) =>
    ({type: SET_MESSAGES, messages: messages});

export const setLikes = (likes) =>
    ({type: SET_LIKES, likes: likes});

export const setIsFetching = (isFetching) =>
    ({type: SET_IS_FETCHING, isFetching: isFetching});

export const setIsShowModal = (isShowModal) =>
    ({type: SET_IS_SHOW_MODAL, isShowModal: isShowModal});

export const getMessages = () => {
  return async (dispatch) => {
    dispatch(setIsFetching(true));
    let res = await chatAPI.getChatData();
    sortByTime(res);
    dispatch(setMessages(res));
    dispatch(setIsFetching(false));
  }
};

export default messengerReducer;
