import {chatAPI} from "../api/api";

const SET_USER = 'SET_USER';
const SET_IS_FETCHING = 'SET_IS_FETCHING';

let initialState = {
  user: {
    id: null,
    name: null,
    avatar: null,
  },
  isFetching: false,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER:
    {
      return {
        ...state,
        user: {...action.user}
      };
    }
    case SET_IS_FETCHING:
    {
      return {
        ...state,
        isFetching: action.isFetching
      };
    }
    default:
      return state;
  }
};

export const setUser = (user) =>
    ({type: SET_USER, user: user});

export const setIsFetching = (isFetching) =>
    ({type: SET_IS_FETCHING, isFetching: isFetching});

export const getUser = () => {
  return async (dispatch) => {
    dispatch(setIsFetching(true));
    let res = await chatAPI.getChatData();
    const user = {
      id: res[res.length-1].userId,
      name: res[res.length-1].user,
      avatar: res[res.length-1].avatar,
    }
    dispatch(setUser(user));
    dispatch(setIsFetching(false));
  }
};

export default userReducer;
